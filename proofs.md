### Convergence of the importance sampling parameters.

**Lemma 1.1**

>  Under (0.2), the function $v^f$ is infinitely continously differentiable with, for all $\alpha=(\alpha^1, \cdots, \alpha^d) \in \mathbb N ^d$ and all $\theta = (\theta^1, \cdots, \theta^d) \in \mathbb R^d$

$$
\frac{\partial^{\alpha_a+\dots + \alpha^d}}{\partial_{\theta_1}^{\alpha^1}\dots\partial_{\theta^d}^{\alpha^d}} v^f(\theta) = \mathbb E \left[\frac{\partial^{\alpha_a+\dots + \alpha^d}}{\partial_{\theta_1}^{\alpha^1}\dots\partial_{\theta^d}^{\alpha^d}}\left[f^2(G)e^{-\theta.G + |\theta|^2/2}\right]\right]
$$

>  Under (0.1), the function $v^f$ is strongly convex and hence is such that $\lim_{|\theta|\to + \infty} v^f(\theta) = + \infty$.



**Reinforced integrability condition**
$$
\forall \theta \in \mathbb{R}^d \quad \mathbb{E}(f^4(G)e^{-\theta.G}) < + \infty
$$
**Proposition 1.2**

Under (0.1) and (0.2), 
$$
\begin{cases}
\nu_n^{f,A}& \overset{a.s}{\longrightarrow}& \nu_\star^{f,A} \\
v_n^{f,A}(\nu_n^{f,A}) &\overset{a.s}{\longrightarrow}&v_n^{f,A}(\nu_\star^{f,A}) 
\end{cases}
$$
If moreover (1.2) holds, then
$$
\sqrt{n}(\nu_n^{f,A} - \nu_\star^{f,A}) \overset{\mathcal L}{\longrightarrow} \mathcal{N}_{d'}(0,\Gamma) \quad \quad \Gamma \succeq 0
$$
**Lemma 1.4**: Uniform law of large numbers

Let $(X_i)_{i\geq 1}$ be a sequence of i.i.d $\mathbb R^m$ valued random vector and $h: \mathbb{R}^d \times \mathbb{R}^m \to \mathbb R$ be a measurable function. Assume that:

- a.s., $\theta \in \mathbb{R}^d \mapsto h(\theta, X_1)$ is continuous
- $\forall M> 0, \mathbb E [\sup_{|\theta| \leq  M} |h(\theta, X_1)|]  < + \infty $

then, a.s, $\theta \in \mathbb{R}^d \mapsto \frac{1}{n} \sum_{i=1}^n h(\theta,X_i)$ converges locally uniformly to the continuous function $\theta \in \mathbb{R}^d \mapsto \mathbb{E}(h(\theta,X_1))$



###Strong law of large numbers and central limit theorem


$$
\theta_n^{f,A} = A \nu_n^{f,A} \quad \quad\text{and}\quad\quad\theta_\star^{f,A} = A \nu_\star^{f,A}
$$

$$
\forall \theta \in \mathbb{R}^d, \quad M_n(\theta, g) \overset{def}{=} \frac{1}{n} \sum_{i=1}^n g(G_i+\theta)e^{-\theta.G_i - |\theta|^2/2}
$$
**Theorem 2.2**

Assume (0.1), (0.2) and that $f$ admits a decomposition
$$
f = f_1 + \mathbf{1}(d ′ =1) + f_2
$$
with $f_1$ a continuous function such that
$$
\forall M > 0, \quad \mathbb{E}\left[\sup_{|\theta| \leq M} |f_1(G+\theta)| + \sup_{|\theta| \leq M} |\nabla f_1(G+\theta)|\right] < + \infty
$$
$f_2 \in \mathcal{V}_A$. Then, for any deterministic integer-valued sequence $(\nu_n)_n$ going to $\infty$ with $n$, $M_n(\theta_{\nu}^{f,A}, f)$  converges a.s. to $\mathbb{E}(g(G))$


$$
\mathcal{H}_\alpha = \{g:\mathbb{R}^d \to \mathbb{R},\  \text{s.t} \  \exists \beta \in [0,2), \lambda> 0, \forall x \in \mathbb{R}^2, |g(x)| \leq \lambda e^{|x|^\beta}, \forall x,y \in \mathbb{R}^d, |g(x) - g(y) | \leq \lambda e^{\max(|x|^\beta, |y|^\beta)}|x - y|^\alpha\}
$$


**Theorem 2.3**

Assume (0.1), (1.2)  and that $f$ admits a decomposition:
$$
f = f_1 + f_2 + \mathbf{1}(d' =1)f_3
$$
with:

- $f_1$ a $C^1$ function such that:
  $$
  \forall M > 0, \quad \mathbb{E}\left[\sup_{|\theta| \leq M} |f_1(G+\theta)| + \sup_{|\theta| \leq M}|\nabla f_1(G+ \theta)|\right] < + \infty
  $$

- $f_2 \in \mathcal{H}_\alpha$ with $\alpha \in \left(\frac{\sqrt{d'^2+8d'} -d'}{4}, 1\right] $ and $f_3 \in \mathcal{V}_A$. Then:

$$
\sqrt{n}(M_n(\theta_n^{f,A}) - \mathbb{E}(f(G))) \overset{\mathcal L}{\longrightarrow} \mathcal{N}_1(0, v^{f,A}(\nu_\star^{f,A}) - \mathbb{E}^2[f(G)])
$$

**Corollary 2.4**

Under the assumptions of Theorem 2.3


$$
\mathbb{V}(f(G)) >0 \implies \sqrt\frac{n}{v_n^{f,A}(\nu_n^{f,A}) - M_n^2(\theta_n^{f,A}, f)} (M_n(\theta_n^{f,A}, f) - \mathbb{E}(f(G))) \overset{\mathcal L}{\longrightarrow} \mathcal{N}_1(0,1)
$$

### The general case



**Proposition 2.6**

Let $(\theta_n)_{n \geq 1} \to \theta_\infty$ and $g:\mathbb{R}^d \to \mathbb{R}$ be a continuous function such that
$$
\forall M > 0,\quad  \mathbb{E}[\sup_{|\theta| \leq M } |g(G+\theta|] < + \infty
$$
then
$$
M_n(\theta_n, g) \overset{a.s}{\longrightarrow} \mathbb{E}(g(G))
$$
**Proposition 2.7**

 Assume that $g: \mathbb{R}^d \to \mathbb{R}$ is such that $\mathbb{E}\left[g^2(G+\theta_\star^{f,A})e^{-2\theta_\star^{f,A}.G}\right] < + \infty$ and admits a decomposition
$$
g = g_1 + g_2
$$
with $g_1$ of class $C_1$ and satisfying


$$
\forall M > 0, \quad \quad \mathbb{E}\left[\sup_{|\theta| \leq M} |g_1(\theta + G) | + \sup_{|\theta| \leq M} | \nabla g_1(\theta+G)| \right] < + \infty
$$
and $g_2 \in \mathcal{H}_\alpha$, for $\alpha \in \left(\frac{\sqrt{d'^2+ 8d'} -d'}{4}, 1\right]$. Then, under (0.1) and (1.2),
$$
\sqrt{n}(M_n(\theta_n^{f,A},g)) - \mathbb{E}\left[g(G)\right]) \overset{\mathcal{L}}{\longrightarrow}  \mathcal{N}_1(0, \mathbb{V}(g(G + \theta_\star^{f,A})e^{-\theta_\star^{f,A}.G - |\theta_\star^{f,A}|^2/2}))
$$
**Lemma 2.8**

Let $g: \mathbb{R}^d \longrightarrow \mathbb{R}$ be a $C^1$ function satisfying (2.2). Then, under (0.1) and (1.2),
$$
\sqrt{n} (M_n(\theta_n^{f,A}, g) - M_n(\theta_\star^{f,A},g)) \overset{Pr}{\rightarrow} 0
$$


**Proposition 2.9**

Letting $\mathcal{A} \in \mathbb{R}^{d \times d'}$ and $g\in \mathcal{H}_\alpha$ for $\alpha \in (0,1]$


$$
\forall \delta > \frac{d'}{2\alpha(d'+2\alpha)}, \forall \nu_0 \in \mathbb{R}^{d'}, \\ \sup_{|\nu - \nu_0| \leq 1/n^\delta} \sqrt{n} |M_n(A\nu, g) - M_n(A\nu_0,g) |\overset{Pr}{\rightarrow} 0
$$
.

**Lemma 2.12**

If $g \in \mathcal{H}_\alpha$ for $\alpha \in (0,1]$, then
$$
\forall M  > 0, \exists C >0, \forall \theta, \theta' \in \bar{B}(0, M), \forall n \in \mathbb{N} - \{0\} \\
\mathbb{E}\left[(M_n(\theta, g) - M_n(\theta',g))^2\right] \leq \frac{C|\theta - \theta'|^{2\alpha}}{n}
$$


**Proposition 2.13**

Let $\mathcal{A} \in$

-----------

## Expérimentations

Si $f$ est une gaussienne de centre $\mu$ et de variance $\sigma^2$ alors $\theta^* = \dfrac{\mu}{\sigma^2 + 1}$.