import numpy as np
import pylab as pl
from mpl_toolkits.mplot3d import Axes3D

fact_mem = [1]
def fact(n):
	while n >= len(fact_mem):
		fact_mem.append(fact_mem[-1]*len(fact_mem))
	return fact_mem[n]

l_coef_mem = [1]
l_mem = {}
def Legendre(l, m, x, start=True):
	global l_mem
	if m > l: return 0
	if m < 0:
		return (-1)**m * fact(l+m) / fact(l-m) * Legendre(l, m, x)
	if start: l_mem = {}
	if l in l_mem: return l_mem[l]
	if l == m:
		while l >= len(l_coef_mem):
			l_coef_mem.append(-l_coef_mem[-1]*(2*len(l_coef_mem)-1))
		l_mem[l] = l_coef_mem[l] * (1 - x**2) ** (0.5*l)
		return l_mem[l]
	l_mem[l] = (Legendre(l-1, m, x, False) * x * (2*l-1) - Legendre(l-2, m, x, False) * (l+m-1)) / (l-m)
	return l_mem[l]

def harmonic(l, m, th, ps):
	return np.sqrt((2*l+1)*fact(l-m)/fact(l+m)/(4*np.pi)) * Legendre(l, m, np.cos(th)) * np.exp(1j * m * ps)

def H(l, m, G):
	G /= np.linalg.norm(G)
	th = np.arccos(G[2])
	ps = np.arccos(G[0]/np.sin(th))
	return 4*np.pi * abs(harmonic(l, m, th, ps)) ** 2

if __name__ == "__main__":
	N = 5000
	l, m = 5, 4
	ths = 2*np.pi * np.random.rand(N)
	pss = np.arccos(2 * np.random.rand(N) - 1)
	rs = np.array([abs(harmonic(l, m, t, p)) for t,p in zip(ths, pss)])
	cs = pl.cm.rainbow(rs / rs.max())[:,:3]

	fig = pl.figure()
	ax = fig.gca(projection='3d')
	x = rs * np.sin(ths) * np.cos(pss)
	y = rs * np.sin(ths) * np.sin(pss)
	z = rs * np.cos(ths)
	ax.scatter(x, y, z, color=cs)
	pl.show()