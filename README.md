# Computational Statistics : Paper 

**Students:**

- Yoann Coudert--Osmont
- Julien Jerphanion

**Chosen article:** Robust Adaptative Importance Samping for Normal Random Vector

**Keywords:**

- Adaptative importance sampling
- Central Limit Theorem
- Sample Averaging

## Introduction:

**Goal** : Estimate
$$
\mathbb{E}[f(G)] \quad G=(G^1, \cdots, G^d) \sim \mathcal{N}(\mathbf{0}_d, I_d)
$$
Applications:

- mathematical finance

  

Assumption on $f$:

- $f: \mathbb{R}^d\mapsto \mathbb{R}$

- $f$ measurable such that $f(G)$ integrable

- $f(G)$ non zero ie
  $$
  \mathbb{P}(f(G) \neq 0) > 0
  $$

- $f(G)$ slightly more than square-integrable: 
  $$
  - \mathbb{E}[f^2(G)e^{-\theta G}] < + \infty
  $$



See [`theorems.md`](./theorems.md) for an overview of theorems and propositions of the paper.

**Results:**

For $(G_i)_{i\geq 1} \sim \mathcal{N}(\mathbf{0}_d, I_d)$, for all $\theta \in \mathbb{R}^d$:


$$
M_n(\theta,f) \overset{\text{def}}{=} \frac 1n \sum_{i=1} f(G_i+ \theta)e^{-\theta.G_i - \vert \theta\vert^2/2} \text{    is an unbiased and convergent estimator of   } \mathbb{E}[f(G)]
$$
As:
$$
n\mathbb{V}[M_n(\theta,f)] = v^f(\theta) - \mathbb{E}^2[f(G)]
$$
where:
$$
v^f(\theta) \overset{def}{=} \mathbb{E}[f^2(G)e^{-\theta G + \vert \theta \vert^2/2}]
$$


we want:
$$
\theta^\star \in \text{Argmin}_\theta v^f(\theta)
$$

Mais généralement $\mathbb{E}[f(G)]$ est inconnue donc $v^f(\theta)$ l'est aussi.

On a de manière générale:
$$
\nabla_\theta v^f(\theta^*) = \mathbf{0}
$$
Plusieurs pistes : 

1. Robbins-Monro procédure sur 

$$
\nabla_\theta v^f(\theta) = \mathbb{E}[(\theta - G)f^2(G)e^{-\theta G + |\theta|^2/2}]
$$

2. Robbins-Monro procédure sur 

$$
\nabla_\theta v^f(\theta) = e^{|\theta|^2}\mathbb{E}[(2\theta-G)f^2(G- \theta)]
$$

Mais Robbins-Monro est dur à tuner.



**Proposition de ce papier :**

Proposer une approche alternative qui ne nécessite pas de tuner cette procédure — d'où le nom de "Robust Adaptive Importance Sampling".

Se base sur l'approximation via Monte Carlo de $v^f(\theta)$ :
$$
v_n^f(\theta) = \frac 1n \sum_{i=1}^n f^2(G_i) e^{-\theta .G_i + |\theta|^2/2}
$$
Cette fonction est fortement convexe et de limite infinie à l'infini — on a donc bien un unique minimiseur que l'on notera $\theta_n^f$:
$$
\theta_n^f = \text{argmin}_{\theta \in \mathbb{R}^d} v_n^f(\theta)
$$
Pour cela, on réalise des méthodes de Newton avec:
$$
\left\{
\begin{matrix}
	\nabla_\theta v_n^f(\theta) &= &\frac 1n \sum_{i=1}^n(\theta - G_i)f^2(G_i)e^{-\theta.G_i + |\theta |^2/2 } \\
	\nabla_\theta^2 v_n^f(\theta) &= &\frac 1n \sum_{i=1}^n\left[I_d+(\theta - G_i)(\theta - G_i)^\top\right] f^2(G_i)e^{-\theta.G_i + |\theta |^2/2 }
\end{matrix}
\right.
$$

---

En pratique, on recherche $\theta$ sur un sous-ensemble de $\mathbb{R}^d$
$$
\{A\mathcal{\nu}\ |\ \nu \in \mathbb{R}^{d'}\} 
$$
avec $A\in \mathbb{R}^{d\times d'}$ of rank $d' \leq d$.

 

Ainsi, on se ramène à l'étude de $v^{f,A}$ définie par :
$$
v^{f,A} : \nu \longmapsto v^f(A\nu)
$$
Dans le cas $A=I_d \in \mathbb{R}^{d \times d}$, on retombe dans le cas initial, i.e. $v^{f,A} = v^f$.

Dans le cas réduit on parle de *Reduced Robust Importance Sampling* (RRIS); dans ce dernier cas, on parle de *Robust Importance Sampling* (RIS). En pratique la variance du RRIS et du RIS est quasiment identique, d'où la motivation d'utiliser le RIS.

---



Si on traduit cela en terme d'approximation, on obtient:
$$
\nabla_\nu v_n^{f,A}(\nu) = A^*A\nu \frac 1n \sum_{i=1}^nf^2(G_i)e^{-A\nu.G_i + |A\nu|^2/2} - \frac 1n \sum_{i=1}^n A^* G_if^2(G_i)e^{-A\nu.G_i + |A\nu|^2/2}
$$
Annuler $\nabla_\nu v_n^{f,A}(\nu) $ revient à annuler:
$$
A^*A\nu - \frac{\sum_{i=1}^n A^* G_if^2(G_i)e^{-A\nu . G_i}}{\sum_{i=1}^n f^2(G_i)e^{-A\nu . G_i}}
$$
On s'intéresse alors à la fonction $u_n^{f,A}$ définie par:
$$
u_n^{f,A}: \nu \longmapsto \frac{\left\vert A \nu \right\vert^2}{2} + \log \left(\sum_{i=1}^n f^2(G_i)e^{-A\nu.G_i}\right) 
$$
On a alors:
$$
\left\{
\begin{matrix}
	\nabla_\theta u_n^f(\theta) &=& A^*A\nu &-& \frac{\sum_{i=1}^n A^* G_if^2(G_i)e^{-A\nu . G_i}}{\sum_{i=1}^n f^2(G_i)e^{-A\nu . G_i}}&\\
	\nabla_\theta^2 u_n^f(\theta) &=& A^*A &+& \frac{\sum_{i=1}^n A^* G_iG_i^*Af^2(G_i)e^{-A\nu . G_i}}{\sum_{i=1}^n f^2(G_i)e^{-A\nu . G_i}} &-& \frac{\left(\sum_{i=1}^nA^*G_if^2(G_i)e^{-A\nu.G_i}\right)\ \left(\sum_{i=1}^nA^*G_if^2(G_i)e^{-A\nu.G_i}\right)^*}{\left(\sum_{i=1}^nf^2(G_i)e^{-A\nu.G_i}\right)^2}
\end{matrix}
\right.
$$


**Quelques remarques très importantes**

Les étapes d'estimation et d'optimisation se basent sur les mêmes réalisations des vecteurs $(G_i)_{i \leq n}$.



**Plan de l'article**

1. Convergence des paramètres de l'importance sampling pour cette méthode
2. Loi forte des grands nombres et théorème limite centrale pour cette méthode


