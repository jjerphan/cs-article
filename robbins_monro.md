---



## A Stochastic Approximation Method (Robbins, Monro)



Study of an unknown function: $x\mapsto M(x)$ s.t.

- denote the expected value at the level $x$ of the result to a certain experiment.
- $M(x) = \alpha \iff x = \theta$



**Idea:**

Based on evaluation on previous values $\{x_1, \dots, x_{n-1}\}$ the have:

- $\{M(x_1), \dots, M(x_{n-1)}\}$
- $\{M'(x_1), \dots, M'(x_{n-1)}\}$ potentially



The method is effective if:
$$
\lim_{n \to + \infty} x_n = \theta \quad \text{for all initial values}\quad (x_1,x_2, \dots, x_r)
$$
with the speed of convergence being analysable on practical measures.



**More general scope**

$x_i$ is the realisation of a r.v $Y=Y(x_i)$ of cdf 
$$
y\mapsto H(y|x_i)
$$
and of pdf
$$
y \mapsto h(y|x_i) = \text{d} H(y|x_i)
$$
such that:
$$
\mathbb E[ Y] = \int_{-\infty}^{+\infty} y\ h(y|x)\ \text{d}y = M(x)
$$
**Idea**: Estimate $\theta$ by making successive observation on $Y$ at levels $x_i, x_2, \dots$

If we have the effectiveness of the method in probability, the procedure is said *consistent* for $y \mapsto H(y|x)$ and value $\alpha$

**Assumptions**

- $\mathbb{P}(|Y| \leq C) = \int_{-C}^C h(y|x) \text{d} y= 1$
- $\exists \alpha, \theta, st. M(x) \leq \alpha \quad \text{for} \quad x < \theta, \quad  M(x) \geq \alpha \quad \text{for}\quad x > \theta $

We consider:

- $(a_n)_n\quad \text{s.t}\quad \sum_{i=1}^\infty a_n^2 = A < \infty$
- $(x_n)_n$ a Markov Chain with $x_0$ fixed, $x_{n+1} - x_n = a_n(\alpha - y_n)$
- $(y_n)_n$ r.v such that $\mathbb{P}(y_n \leq y |x_n) = H(y|x_n)$
- $(b_n)_n \quad \text{s.t} \quad \mathbb{E}[x_n- \theta]^2$

We can to get:
$$
\lim_{n\to \infty} b_n = 0
$$
We have:


$$
\begin{align*}
b_{n+1} &= \mathbb{E}(x_{n-1} - \theta)^2 = \mathbb{E}[\mathbb E[(x_{n+1} - \theta)^2 | x_n]]\\
&= \mathbb{E}\left[\int_{-\infty}^{\infty}\left[(x_n - \theta) - a_n(y - \alpha)\right]^2 h(y|x)\ \text{d} y\right] \\
&= b_n + a_n²
\end{align*}
$$